## [1.2.0](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.4...1.2.0) (2023-11-16)


### :sparkles: Features

* Add panel button ([00e45ee](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/00e45ee26f600ca9749dad57ceb4aa05aa76cabf))


### :arrow_up: Upgrade dependencies

* **deps:** update openssl to 3.1.4-r1 ([6cbdd99](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/6cbdd99b0cc47628a3bc6a3e7683f9573d8d2b10))

## [1.1.4](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.3...1.1.4) (2023-11-16)


### :memo: Documentation

* add badges ([3d91de0](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/3d91de0cc1a30cdaf63d7cb99425b2af2164004c))


### :construction_worker: CI

* Configure Renovate ([e4db06f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/e4db06ffde81722938581fedb68ebfdcd3b947be))


### :arrow_up: Upgrade dependencies

* **deps:** update dependency @semantic-release/gitlab to v12.1.0 ([0cc9e10](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/0cc9e10f38f903321bd7175c77dab11c4dc83d06))
* **deps:** update dependency prettier to v3.1.0 ([9e3cd9c](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9e3cd9c113071e77a35651adc5bf6a8a3bbd00bc))
* **deps:** update node.js to v21.2.0 ([8f19cb1](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/8f19cb1a00a2709a63a620edff22939ed67779ad))

## [1.1.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.2...1.1.3) (2023-11-13)


### :construction_worker: CI

* kaniko ([9ab0a25](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9ab0a25d1d02d37b89720a43b85eab00897c9261))
* semantic release ([102cc1f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/102cc1fd3dc7ee48c5f9a830d82c34c2120b57c8))


### :arrow_up: Upgrade dependencies

* **deps:** @types/express @types/superagent prettier typescript superagent tslib ([ef5cdad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ef5cdad08a9f7bd9bf30d9b9f5a0c248d14dbaff))
* **deps:** node 21.1.0 ([a413f30](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a413f303b6d1d4ef5d72496623d09a24d615e11b))

## [1.1.3](https://gitlab.com/Saibe1111/grafana-to-ntfy/compare/1.1.2...1.1.3) (2023-11-13)


### :construction_worker: CI

* kaniko ([9ab0a25](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/9ab0a25d1d02d37b89720a43b85eab00897c9261))
* semantic release ([102cc1f](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/102cc1fd3dc7ee48c5f9a830d82c34c2120b57c8))


### :arrow_up: Upgrade dependencies

* **deps:** @types/express @types/superagent prettier typescript superagent tslib ([ef5cdad](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/ef5cdad08a9f7bd9bf30d9b9f5a0c248d14dbaff))
* **deps:** node 21.1.0 ([a413f30](https://gitlab.com/Saibe1111/grafana-to-ntfy/commit/a413f303b6d1d4ef5d72496623d09a24d615e11b))
