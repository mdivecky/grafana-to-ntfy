import { Request, Response } from 'express';
import { GrafanaService } from '../services/GrafanaService';

const grafanaService = new GrafanaService();

export class GrafanaController {
    public async postAlert(req: Request, res: Response): Promise<Response> {
        try {
            console.log('Message forwarding to ntfy in progress ...');
            await grafanaService.postAlert(req);
            console.log('Message successfully forwarded.');
            return res.status(200).send();
        } catch (err) {
            const error = err as Error;
            console.error('An error occurred during the transmission of the message. Error:' + error.message);
            return res.status(500).json({ error: error.message });
        }
    }
}
