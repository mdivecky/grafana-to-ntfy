import { Priority } from '../enum/priority';

export interface MessageHeader {
    priority: Priority;
    title: string;
    tags: string;
    actions: string;
    authorization?: string;
}
