import { MessageHeader } from '../interface/MessageHeader';
import { Priority } from '../enum/priority';
import superagent from 'superagent';

export class NtfyMessage {
    private header: MessageHeader;
    private server: string;
    private message: string;
    private topic: string;

    constructor(title: string, message: string, priority: Priority, tags: string, actions: string) {
        this.header = {
            priority: priority,
            title: title,
            tags: tags,
            actions: actions,
            authorization: ''
        };

        if (process.env['NTFY_TOKEN'] as string) {
            this.header.authorization = ('Bearer ' + process.env['NTFY_TOKEN']) as string;
        }

        this.message = message;
        this.topic = (process.env['NTFY_TOPIC'] as string) || 'grafana';
        this.server = (process.env['NTFY_SERVER'] as string) || 'https://ntfy.sh';
    }

    public async publish(): Promise<superagent.Response> {
        const response = await superagent
            .post(this.server + '/' + this.topic)
            .set(this.transformMessageHeaderToObj(this.header))
            .send(this.message);
        return response;
    }

    private transformMessageHeaderToObj(header: MessageHeader): { [key: string]: string } {
        const obj: { [key: string]: string } = {};
        for (const key in header) {
            if (header.hasOwnProperty(key)) {
                obj[key] = String(header[key as keyof typeof header]);
            }
        }
        return obj;
    }
}
