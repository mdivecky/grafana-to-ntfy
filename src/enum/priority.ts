export enum Priority {
    URGENT = 'urgent',
    HIGH = 'high',
    DEFAULT = 'default',
    LOW = 'low',
    MIN = 'min'
}
