import express, { Application } from 'express';
import { indexRouter } from './routes';
import * as dotenv from 'dotenv';
dotenv.config();

const app: Application = express();
app.use(express.json());

app.use('/', indexRouter);

const PORT = 1111;
app.listen(PORT, () => {
    const topic = (process.env['NTFY_TOPIC'] as string) || 'grafana';
    const server = (process.env['NTFY_SERVER'] as string) || 'https://ntfy.sh';
    console.log(`The server listens on the port: ${PORT}`);
    console.log(`All alerts will be forwarded to: ${server}/${topic}`);
});
