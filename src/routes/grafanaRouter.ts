import { Router } from 'express';
import { GrafanaController } from '../controllers/GrafanaController';

export const grafanaRouter: Router = Router();
const grafanaController: GrafanaController = new GrafanaController();

grafanaRouter.post('/', grafanaController.postAlert);
