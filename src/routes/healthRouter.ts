import { Router } from 'express';
import { HealthController } from '../controllers/HealthController';

export const healthRouter: Router = Router();
const heathController: HealthController = new HealthController();

healthRouter.get('/', heathController.getHealth);
