import { Request } from 'express';
import { NtfyMessage } from '../models/NtfyMessage';
import { Priority } from '../enum/priority';

export class GrafanaService {
    public async postAlert(req: Request): Promise<void> {
        const messagePriotity = getPriority(req.body.status, req.body.commonLabels.priority);
        const messageTitle = req.body.commonLabels.alertname;
        const messageDescription = req.body.commonAnnotations.summary;
        const messageTags = getTags(req.body.commonLabels, req.body.status);
        const messageActions = getActions(req.body.alerts[0].silenceURL, req.body.alerts[0].dashboardURL, req.body.alerts[0].panelURL);
        const message = new NtfyMessage(messageTitle, messageDescription, messagePriotity, messageTags, messageActions);
        const response = await message.publish();
        if (!response.ok) {
            if (response.status == 403) {
                throw new Error('Forbidden');
            }
            throw new Error('Unpublishable');
        }
    }
}

function getPriority(status: string, priority: string): Priority {
	if (status == 'resolved') {
		return Priority.MIN;
	}

	switch (priority) {
		case 'low':
			return Priority.MIN;
		case 'normal':
			return Priority.DEFAULT;
		case 'high':
			return Priority.HIGH;
		case 'critical':
			return Priority.URGENT;
		default:
			return Priority.DEFAULT;
	}
}

function getTags(commonLabels: string, status: string): string {
    const values = Object.values(commonLabels);
    const str = values.join(', ');

    switch (status) {
        case 'firing':
            return str + ',rotating_light';
        case 'resolved':
            return str + ',adhesive_bandage';
        default:
            return str;
    }
}

function getActions(silenceURL: string, dashboardURL: string, panelURL: string): string {
    let value = 'view, Silence, ' + silenceURL + ', clear=true;';
    if (dashboardURL) {
        value += 'view, Dashboard, ' + dashboardURL + ', clear=true;';
    }
    if (panelURL) {
        value += 'view, Panel, ' + panelURL + ', clear=true;';
    }
    return value;
}
