<img src="/docs/static/img/logo.png" alt="Logo">

# Grafana to Ntfy

[![Discord](https://img.shields.io/badge/Discord-%235865F2.svg?style=for-the-badge&logo=discord&logoColor=white)](https://discord.gg/qZYJ6fGaZ5)
[![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)](https://hub.docker.com/r/saibe1111/grafana-to-ntfy)
[![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/Saibe1111/grafana-to-ntfy/)

-   [Ntfy](https://ntfy.sh/)
-   [Grafana](https://grafana.com/)

## Summary

-   [Example](#example)
-   [Installation](#installation)
    -   [Docker compose](#docker-compose)
    -   [Variables](#variables)
-   [Grafana configuration](#grafana-configuration)
-   [Setup a Grafana rule](#setup-a-grafana-rule)
    -   [Title](#title)
    -   [Description](#description)
    -   [Tags](#tags)
    -   [Dashboard button](#dashboard-button)

## Example

Here is an example of a notification.

<img src="/docs/static/img/demo.png" alt="Example of a notification">

## Installation

### Docker compose

```yml
version: '3.8'

services:
    grafana:
        container_name: grafana
        image: grafana/grafana
        restart: always
        ports:
            - '3003:3000'

    grafana-to-ntfy:
        container_name: grafana-to-ntfy
        image: saibe1111/grafana-to-ntfy
        restart: unless-stopped
        environment:
            NTFY_TOPIC: grafana-custom-alert-topic
```

### Variables

| **Variable** | **Usage**                                                        | **Default value** | **Required** |
| :----------: | ---------------------------------------------------------------- | :---------------: | :----------: |
|  NTFY_TOPIC  | Topic name                                                       |      grafana      |      ❌      |
| NTFY_SERVER  | URL of the ntfy server                                           |  https://ntfy.sh  |      ❌      |
|  NTFY_TOKEN  | Access tokens [Docs](https://docs.ntfy.sh/config/#access-tokens) |         /         |      ❌      |

## Grafana configuration

You just need to add a webhook contact point as below:

<img src="/docs/static/img/grafana-config.png" alt="Grafana">

## Setup a Grafana rule

### Title

The title is the name of the Grafana alert.

<img src="/docs/static/img/grafana-config/ntfy-title.jpg" alt="Grafana">

<img src="/docs/static/img/grafana-config/grafana-title.png" alt="Grafana">

### Description

The description is based on the Grafana summary.

<img src="/docs/static/img/grafana-config/ntfy-summary.jpg" alt="Grafana">

<img src="/docs/static/img/grafana-config/grafana-summary.png" alt="Grafana">

### Tags

Tags are the name of the alert, the name of the folder and the labels.

<img src="/docs/static/img/grafana-config/ntfy-tags.jpg" alt="Grafana">

<img src="/docs/static/img/grafana-config/grafana-tags-1.png" alt="Grafana">
<img src="/docs/static/img/grafana-config/grafana-tags-2.png" alt="Grafana">

### Dashboard button

The "Dashboard" button corresponds to a Grafana dashboard.

<img src="/docs/static/img/grafana-config/ntfy-dashboard.jpg" alt="Grafana">

<img src="/docs/static/img/grafana-config/grafana-dashboard.png" alt="Grafana">
